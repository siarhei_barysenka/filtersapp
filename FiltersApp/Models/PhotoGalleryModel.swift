//
//  PhotoGalleryModel.swift
//  FiltersApp
//
//  Created by Siarhei Barysenka on 8/1/15.
//  Copyright (c) 2015 Siarhei Barysenka. All rights reserved.
//

import Foundation
import AssetsLibrary
import ObjectiveC

class PhotoGalleryModel {
  
  var assetsLibrary: ALAssetsLibrary!
  
  // MARK: Initialization
  init() {
    self.assetsLibrary = ALAssetsLibrary()
  }
  
  // MARK: Public
  func retrieveAssets(success: (assets: [ALAsset]) -> Void, failure: (error: NSError) -> Void) {
    var assets: [ALAsset] = []
    
    self.assetsLibrary.enumerateGroupsWithTypes(ALAssetsGroupSavedPhotos, usingBlock: { (group: ALAssetsGroup!, stop: UnsafeMutablePointer<ObjCBool>) -> Void in
      if let incomingGroup = group {
        incomingGroup.enumerateAssetsUsingBlock({ (asset: ALAsset!, index: Int, stop: UnsafeMutablePointer<ObjCBool>) -> Void in
          if let incomingAsset = asset {
            assets.append(incomingAsset)
          }
        })
      } else {
        // From the ALAssetsLibrary header:
        // "When the enumeration is done, 'enumerationBlock' will be called with group set to nil."
        success(assets: assets)
      }
    }, failureBlock: { (error: NSError!) -> Void in
      failure(error: error)
    })
  }
  
  func sortAssets(assetsToSort: [ALAsset], completion: ((sortedAssets: [ALAsset]) -> Void)) {
    var assets = assetsToSort
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
      assets.sort { (firstAsset: ALAsset, secondAsset: ALAsset) -> Bool in
        let firstDate = firstAsset.valueForProperty("ALAssetPropertyDate") as! NSDate
        let secondDate = secondAsset.valueForProperty("ALAssetPropertyDate") as! NSDate
        return firstDate.compare(secondDate) == .OrderedDescending
      }
      
      completion(sortedAssets: assets)
    })
  }
  
  func fullScreenImageFromAsset(asset: ALAsset?, completion: ((image: UIImage?) -> Void)) {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { [weak self] () -> Void in
      if let imageRef = asset?.defaultRepresentation().fullScreenImage().takeUnretainedValue() {
        let image = UIImage(CGImage: imageRef)
        completion(image: image)
      } else {
        completion(image: nil)
      }
    })
  }
}
