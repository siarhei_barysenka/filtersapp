//
//  ImageProcessingModel.swift
//  FiltersApp
//
//  Created by Siarhei Barysenka on 8/3/15.
//  Copyright (c) 2015 Siarhei Barysenka. All rights reserved.
//

import UIKit
import GPUImage

class ImageProcessingModel {
  
  var originalImage: UIImage!
  var filter: GPUImageFilter!
  
  // MARK: Initialization
  required init(image: UIImage, filter: GPUImageFilter) {
    self.originalImage = image
    self.filter = filter
  }
  
  // MARK: Public
  func processWithAlpha(alpha: CGFloat, completion: ((image: UIImage?) -> Void)) {
    var originalImageSource = GPUImagePicture(image: self.originalImage)
    var filteredImageSource = GPUImagePicture(image: self.originalImage)
    
    var alphaFilter = GPUImageAlphaBlendFilter()
    alphaFilter.mix = CGFloat(1.0 - alpha)
    alphaFilter.useNextFrameForImageCapture()
    
    filteredImageSource.addTarget(self.filter)
    self.filter.useNextFrameForImageCapture()
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { [weak self] () -> Void in
      filteredImageSource.processImageWithCompletionHandler { () -> Void in
        if let processedImage = self?.filter.imageFromCurrentFramebuffer() {
          var mainImageSource = GPUImagePicture(image: processedImage)
          mainImageSource.addTarget(alphaFilter)
          originalImageSource.addTarget(alphaFilter)
          mainImageSource.processImage()
          originalImageSource.processImage()
          
          var finalImage = alphaFilter.imageFromCurrentFramebuffer()
          completion(image: finalImage)
        } else {
          completion(image: nil)
        }
      }
    })
  }
  
}
