//
//  GalleryViewController.swift
//  FiltersApp
//
//  Created by Siarhei Barysenka on 8/1/15.
//  Copyright (c) 2015 Siarhei Barysenka. All rights reserved.
//

import UIKit
import AssetsLibrary

struct GalleryViewControllerConstants {
  static let ThumbnailCellReuseID = "ThumbnailCellReuseID"
  static let ContentInset: UIEdgeInsets = UIEdgeInsetsMake(20, 4, 50, 4)
  static let ScrollIndicatorInset: UIEdgeInsets = UIEdgeInsetsMake(20, 0, 50, 0)
}

protocol GalleryViewControllerDelegate: class {
  func galleryViewControllerDidSelectAsset(galleryViewController: GalleryViewController, asset: ALAsset?)
}

class GalleryViewController: UIViewController, GalleryViewModelDelegate, UICollectionViewDataSource, UICollectionViewDelegate {
  
  var viewModel: GalleryViewModel?
  weak var delegate: GalleryViewControllerDelegate?
  
  @IBOutlet weak var collectionView: UICollectionView!
  @IBOutlet weak var noPhotosAvailableLabel: UILabel!
  @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
  
  // MARK: View Controller Lifecycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.viewModel = GalleryViewModel()
    self.viewModel!.delegate = self
    self.viewModel!.fetchImages()
    
    self.collectionView.contentInset = GalleryViewControllerConstants.ContentInset
    self.collectionView.scrollIndicatorInsets = GalleryViewControllerConstants.ScrollIndicatorInset
  }
  
  // MARK: GalleryViewModelDelegate
  func galleryViewModelDidStartFetchingImages(galleryViewModel: GalleryViewModel) {
    self.activityIndicator.startAnimating()
    self.noPhotosAvailableLabel.hidden = true
  }
  
  func galleryViewModelDidFinishFetchingImagesWithError(galleryViewModel: GalleryViewModel, error: NSError?) {
    self.activityIndicator.stopAnimating()
    self.noPhotosAvailableLabel.hidden = (0 != galleryViewModel.numberOfImages())
    
    if let completionError = error {
      UIAlertView(title: "Gallery", message: completionError.localizedDescription, delegate: nil, cancelButtonTitle: "OK").show()
    } else {
      self.collectionView.reloadData()
      self.updateSelectedCell()
    }
  }
  
  // MARK: UICollectionViewDataSource
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return self.viewModel!.numberOfImages()
  }
  
  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    var image = self.viewModel!.thumbnailAtIndex(indexPath.item)
    var cell = collectionView.dequeueReusableCellWithReuseIdentifier(GalleryViewControllerConstants.ThumbnailCellReuseID, forIndexPath: indexPath) as! ThumbnailCell
    cell.fillWithImage(image)
    
    return cell
  }
  
  // MARK: UICollectionViewDelegate
  func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
    self.viewModel?.selectedIndex = indexPath.item
    self.callDidSelectImageHandlerWithSelectedIndex(indexPath.item)
  }
  
  // MARK: Private
  private func updateSelectedCell() {
    if let selectedIndex = self.viewModel?.selectedIndex {
      let indexPath = NSIndexPath(forItem: selectedIndex, inSection: 0)
      self.collectionView.selectItemAtIndexPath(indexPath, animated: false, scrollPosition: UICollectionViewScrollPosition.None)
      self.callDidSelectImageHandlerWithSelectedIndex(selectedIndex)
    }
  }
  
  private func callDidSelectImageHandlerWithSelectedIndex(selectedIndex: Int) {
    let selectedAsset = self.viewModel?.assetAtIndex(selectedIndex)
    self.delegate?.galleryViewControllerDidSelectAsset(self, asset: selectedAsset)
  }
}
