//
//  GalleryLayout.swift
//  FiltersApp
//
//  Created by Siarhei Barysenka on 8/1/15.
//  Copyright (c) 2015 Siarhei Barysenka. All rights reserved.
//

import UIKit

struct GalleryLayoutConstants {
  static let NumberOfThumbnailsInRow = 4
  static let MinimumLineSpacing: CGFloat = 2.0
  static let MinimumInteritemSpacing: CGFloat = 2.0
}

class GalleryLayout: UICollectionViewFlowLayout {
  
  // MARK: Initialization
  required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    self.scrollDirection = UICollectionViewScrollDirection.Vertical;
    self.minimumInteritemSpacing = GalleryLayoutConstants.MinimumInteritemSpacing;
    self.minimumLineSpacing = GalleryLayoutConstants.MinimumLineSpacing;
    self.collectionView?.pagingEnabled = false;
  }
  
  // MARK: UICollectionViewFlowLayout
  override var itemSize : CGSize {
    set { }
    get {
      if let collectionView = self.collectionView {
        var contentWidth = collectionView.frame.size.width - collectionView.contentInset.left - collectionView.contentInset.right
        var itemDim = floor((contentWidth - CGFloat(GalleryLayoutConstants.NumberOfThumbnailsInRow - 1) * GalleryLayoutConstants.MinimumInteritemSpacing) / CGFloat(GalleryLayoutConstants.NumberOfThumbnailsInRow))
        return CGSizeMake(itemDim, itemDim)
      } else {
        return CGSizeZero
      }
    }
  }
}
