//
//  ThumbnailCell.swift
//  FiltersApp
//
//  Created by Siarhei Barysenka on 8/1/15.
//  Copyright (c) 2015 Siarhei Barysenka. All rights reserved.
//

import UIKit

struct ThumbnailCellConstants {
  static let BorderColor = UIColor(red: 237.0 / 255.0, green: 88.0 / 255.0, blue: 108.0 / 255.0, alpha: 1.0)
  static let BorderWidth: CGFloat = 2.0
  static let CornerRadius: CGFloat = 4.0
  static let Alpha: CGFloat = 0.6
}

class ThumbnailCell: UICollectionViewCell {
  
  @IBOutlet weak var thumbnailImageView: UIImageView!
  
  // MARK: Initialization
  required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    self.contentView.layer.borderColor = ThumbnailCellConstants.BorderColor.CGColor
    self.contentView.layer.masksToBounds = true
  }
  
  // MARK: UIView
  override func prepareForReuse() {
    super.prepareForReuse()
    self.contentView.layer.borderWidth = 0.0
    self.contentView.layer.cornerRadius = 0.0;
    self.thumbnailImageView.alpha = 1.0;
  }
  
  override var selected: Bool {
    didSet {
      self.contentView.layer.borderWidth = selected ? ThumbnailCellConstants.BorderWidth : 0.0
      self.contentView.layer.cornerRadius = selected ? ThumbnailCellConstants.CornerRadius : 0.0;
      self.thumbnailImageView.alpha = selected ? ThumbnailCellConstants.Alpha : 1.0;
    }
  }
  
  // MARK: Public
  func fillWithImage(image: UIImage?) {
    self.thumbnailImageView.image = image
  }
}
