//
//  FiltersTabBarController.swift
//  FiltersApp
//
//  Created by Siarhei Barysenka on 8/1/15.
//  Copyright (c) 2015 Siarhei Barysenka. All rights reserved.
//

import UIKit
import AssetsLibrary

class FiltersTabBarController: UITabBarController, GalleryViewControllerDelegate, FilterViewControllerDelegate {

  weak var galleryViewController: GalleryViewController?
  weak var filterViewController: FilterViewController?
  
  // MARK: View Controller Lifecycle
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.galleryViewController = self.customizableViewControllers?.first as? GalleryViewController
    self.filterViewController = self.customizableViewControllers?.last as? FilterViewController
    
    self.galleryViewController?.delegate = self
    self.filterViewController?.delegate = self
  }
  
  // MARK: GalleryViewControllerDelegate
  func galleryViewControllerDidSelectAsset(galleryViewController: GalleryViewController, asset: ALAsset?) {
    self.filterViewController?.updateWithAsset(asset)
  }
  
  // MARK: FilterViewControllerDelegate
  func filterViewControllerDidChangeSliderValue(filterViewController: FilterViewController, value: Float) {
    let tabBarItem = self.tabBar.items?.last as? UITabBarItem
    tabBarItem?.badgeValue = "\(Int(value * 100))";
  }
}
