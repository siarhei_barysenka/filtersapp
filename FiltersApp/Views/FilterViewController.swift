//
//  FilterViewController.swift
//  FiltersApp
//
//  Created by Siarhei Barysenka on 8/1/15.
//  Copyright (c) 2015 Siarhei Barysenka. All rights reserved.
//

import UIKit
import AssetsLibrary

protocol FilterViewControllerDelegate: class {
  func filterViewControllerDidChangeSliderValue(filterViewController: FilterViewController, value: Float)
}

class FilterViewController: UIViewController, FilterViewModelDelegate {
  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var alphaSlider: UISlider!
  @IBOutlet weak var noPhotoSelectedLabel: UILabel!
  @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
  
  var viewModel: FilterViewModel?
  weak var delegate: FilterViewControllerDelegate?
  
  // MARK: View Controller Lifecycle
  override func viewDidLoad() {
    super.viewDidLoad()
    
    if self.viewModel == nil {
      self.viewModel = FilterViewModel(asset: nil)
      self.viewModel?.delegate = self
    }
    
    self.noPhotoSelectedLabel.hidden = self.viewModel!.hasSelectedPhoto()
    self.alphaSlider.enabled = self.viewModel!.isPhotoLoaded()
    self.alphaSlider.value = self.viewModel!.alphaValue
  }

  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    let shouldReloadImage = self.viewModel?.shouldReloadImage() ?? false
    if shouldReloadImage {
      self.viewModel?.reloadImage()
    }
  }
  
  // MARK: Public
  func updateWithAsset(asset: ALAsset?) {
    if let viewModel = self.viewModel {
      viewModel.updateAsset(asset)
    } else {
      self.viewModel = FilterViewModel(asset: asset)
      self.viewModel?.delegate = self
    }
  }
  
  @IBAction func didChangeAlphaValue(sender: UISlider) {
    var alpha = sender.value
    self.delegate?.filterViewControllerDidChangeSliderValue(self, value: alpha)
    self.viewModel?.alphaValue = alpha
    self.viewModel?.processImageWithAlpha(self.viewModel!.alphaValue)
  }
  
  // MARK: FilterViewModelDelegate
  func filterViewModelDidStartRetrievingImageFromAsset(viewModel: FilterViewModel) {
    self.activityIndicator.startAnimating()
    self.noPhotoSelectedLabel.hidden = true
    self.alphaSlider.enabled = false
    self.imageView.image = nil
  }
  
  func filterViewModelDidFinishRetrievingImageFromAsset(viewModel: FilterViewModel, image: UIImage?, error: NSError?) {
    self.activityIndicator.stopAnimating()
    self.noPhotoSelectedLabel.hidden = self.viewModel!.hasSelectedPhoto()
    self.alphaSlider.enabled = self.viewModel!.isPhotoLoaded()
    
    if let completionError = error {
      UIAlertView(title: "Filter", message: completionError.localizedDescription, delegate: nil, cancelButtonTitle: "OK").show()
    }
    
    if let retrievedImage = image {
      //self.imageView.image = retrievedImage
      self.viewModel?.processImageWithAlpha(self.viewModel!.alphaValue)
    }
  }
  
  func filterViewModelDidStartProcessingImage(viewModel: FilterViewModel) {
    
  }
  
  func filterViewModelDidFinishProcessingImage(viewModel: FilterViewModel, image: UIImage?, error: NSError?) {
    if let completionError = error {
      UIAlertView(title: "Filter", message: completionError.localizedDescription, delegate: nil, cancelButtonTitle: "OK").show()
    }
    
    if let processedImage = image {
      self.imageView.image = processedImage
    }
  }
}

