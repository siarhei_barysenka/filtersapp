//
//  FilterViewModel.swift
//  FiltersApp
//
//  Created by Siarhei Barysenka on 8/1/15.
//  Copyright (c) 2015 Siarhei Barysenka. All rights reserved.
//

import UIKit
import AssetsLibrary
import GPUImage

protocol FilterViewModelDelegate: class {
  func filterViewModelDidStartRetrievingImageFromAsset(viewModel: FilterViewModel)
  func filterViewModelDidFinishRetrievingImageFromAsset(viewModel: FilterViewModel, image: UIImage?, error: NSError?)
  func filterViewModelDidStartProcessingImage(viewModel: FilterViewModel)
  func filterViewModelDidFinishProcessingImage(viewModel: FilterViewModel, image: UIImage?, error: NSError?)
}

struct Error {
  static let Domain = "FilterAppDomain"
  static let ReloadingImageDescription = "Could not retrieve image from asset."
  static let ProcessingImageDescription = "An error occured while processing image."
  static let ReloadingImageCode = 90091
  static let ProcessingImageCode = 90092
}

class FilterViewModel {
  
  var photoGalleryModel: PhotoGalleryModel!
  var imageProcessingModel: ImageProcessingModel?
  var asset: ALAsset?
  var incomingAsset: ALAsset?
  var originalImage: UIImage?
  var processedImage: UIImage?
  var isLoading: Bool = false
  var alphaValue: Float = 1.0
  
  weak var delegate: FilterViewModelDelegate?
  
  // MARK: Initialization
  required init(asset: ALAsset?) {
    self.incomingAsset = asset
    self.photoGalleryModel = PhotoGalleryModel()
  }
  
  // MARK: Public
  func reloadImage() {
    if self.isLoading == true {
      return
    }
    
    self.isLoading = true
    
    self.asset = self.incomingAsset
    self.imageProcessingModel = nil
    self.originalImage = nil
    self.processedImage = nil
    
    self.delegate?.filterViewModelDidStartRetrievingImageFromAsset(self)
    self.photoGalleryModel.fullScreenImageFromAsset(self.asset, completion: { [weak self] (image) -> Void in
      var error = (image == nil) ? self?.error(Error.ReloadingImageDescription, domain: Error.Domain, code: Error.ReloadingImageCode) : nil
      if let retrievedImage = image, filter = self?.defaultFilter() {
        self?.imageProcessingModel = ImageProcessingModel(image: retrievedImage, filter: filter)
      }
      self?.callCompleteReloadingHandler(image, error: error)
    })
  }
  
  func processImageWithAlpha(alpha: Float) {
    if self.isPhotoLoaded() == false {
      return
    }
    
    if self.imageProcessingModel == nil {
      return
    }
    
    self.delegate?.filterViewModelDidStartProcessingImage(self)
    
    self.imageProcessingModel!.processWithAlpha(CGFloat(alpha), completion: { [weak self] (image) -> Void in
      var error = (image == nil) ? self?.error(Error.ProcessingImageDescription, domain: Error.Domain, code: Error.ProcessingImageCode) : nil
      self?.callCompleteProcessingHandler(image, error: error)
    })
  }
  
  func shouldReloadImage() -> Bool {
    return self.asset != self.incomingAsset
  }
  
  func hasSelectedPhoto() -> Bool {
    return self.incomingAsset != nil
  }
  
  func isPhotoLoaded() -> Bool {
    return self.originalImage != nil
  }
  
  func updateAsset(asset: ALAsset?) {
    self.incomingAsset = asset
  }
  
  // MARK: Private
  private func defaultFilter() -> GPUImageFilter {
    var filter = GPUImageGaussianBlurFilter()
    filter.blurRadiusInPixels = 100
    return filter
  }
  
  private func callCompleteReloadingHandler(image: UIImage?, error: NSError?) {
    dispatch_async(dispatch_get_main_queue(), { [weak self] () -> Void in
      self?.isLoading = false
      if let retrievedImage = image {
        self?.originalImage = retrievedImage
      }
      self?.delegate?.filterViewModelDidFinishRetrievingImageFromAsset(self!, image: image, error: error)
    })
  }
  
  private func callCompleteProcessingHandler(image: UIImage?, error: NSError?) {
    dispatch_async(dispatch_get_main_queue(), { [weak self] () -> Void in
      if let processedImage = image {
        self?.processedImage = processedImage
      }
      self?.delegate?.filterViewModelDidFinishProcessingImage(self!, image: image, error: error)
    })
  }
  
  private func error(description: String, domain: String, code: Int) -> NSError {
    let errorDetails = [NSLocalizedDescriptionKey : description]
    let error = NSError(domain: domain, code: code, userInfo: errorDetails)
    return error
  }
}
