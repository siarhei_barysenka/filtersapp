//
//  GalleryViewModel.swift
//  FiltersApp
//
//  Created by Siarhei Barysenka on 8/1/15.
//  Copyright (c) 2015 Siarhei Barysenka. All rights reserved.
//

import Foundation
import UIKit
import AssetsLibrary

protocol GalleryViewModelDelegate: class {
  func galleryViewModelDidStartFetchingImages(galleryViewModel: GalleryViewModel)
  func galleryViewModelDidFinishFetchingImagesWithError(galleryViewModel: GalleryViewModel, error: NSError?)
}

class GalleryViewModel {
  
  weak var delegate: GalleryViewModelDelegate?
  var photoGalleryModel: PhotoGalleryModel!
  var assetsAccessQueue: dispatch_queue_t!
  var assets: [ALAsset] = []
  var selectedIndex: Int?
  var isLoading: Bool = false
  
  // MARK: Initialization
  init() {
    self.photoGalleryModel = PhotoGalleryModel()
    self.assetsAccessQueue = dispatch_queue_create("FiltersAppAssetsAccessQueue.\(self.dynamicType)", DISPATCH_QUEUE_CONCURRENT)
  }
  
  // MARK: Public
  func fetchImages() {
    if (self.isLoading == true) {
      return
    }
    
    self.isLoading = true
    self.delegate?.galleryViewModelDidStartFetchingImages(self)
    
    self.photoGalleryModel.retrieveAssets({ [weak self] (assets) -> Void in
      self?.photoGalleryModel.sortAssets(assets, completion: { (sortedAssets) -> Void in
        self?.replaceAssetsWithAssets(sortedAssets)
        self?.configureSelectedIndex()
        self?.callCompletionFetchingHandlerWithError(nil)
      })
    }, failure: { [weak self] (error) -> Void in
      self?.callCompletionFetchingHandlerWithError(error)
    })
  }
  
  func numberOfImages() -> Int {
    return assetsCount()
  }
  
  func thumbnailAtIndex(index: Int) -> UIImage? {
    let asset = assetAtIndex(index)
    return UIImage(CGImage:asset.thumbnail().takeUnretainedValue())
  }
  
  func assetAtIndex(index: Int?) -> ALAsset? {
    if let idx = index {
      return assetAtIndex(idx)
    } else {
      return nil
    }
  }
  
  // MARK: Private
  private func assetsCount() -> Int {
    var assetsCount = 0
    dispatch_sync(self.assetsAccessQueue) { [weak self] () -> Void in
      assetsCount = self?.assets.count ?? 0
    }
    
    return assetsCount
  }
  
  private func assetAtIndex(index: Int) -> ALAsset {
    var asset: ALAsset?
    dispatch_sync(self.assetsAccessQueue, { [weak self] () -> Void in
      asset = self?.assets[index]
    })
    
    return asset ?? ALAsset()
  }
  
  private func replaceAssetsWithAssets(assets: [ALAsset]) {
    dispatch_barrier_async(self.assetsAccessQueue) { [weak self] () -> Void in
      self?.assets.removeAll(keepCapacity: false)
      self?.assets.extend(assets)
    }
  }
  
  private func configureSelectedIndex() {
    if let currentIndex = self.selectedIndex {
      if self.numberOfImages() == 0 {
        self.selectedIndex = nil
      } else if currentIndex >= self.numberOfImages() {
        self.selectedIndex = 0;
      }
    } else {
      if self.numberOfImages() > 0 {
        self.selectedIndex = 0;
      }
    }
  }
  
  private func callCompletionFetchingHandlerWithError(error: NSError?) {
    dispatch_async(dispatch_get_main_queue(), { [weak self] () -> Void in
      self?.isLoading = false
      self?.delegate?.galleryViewModelDidFinishFetchingImagesWithError(self!, error: error)
    })
  }
}
